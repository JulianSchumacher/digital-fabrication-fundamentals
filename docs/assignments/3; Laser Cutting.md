#Laser Cutting

###From design to fabrication
Where the previous assignment focused on digitally designing (more or less) complex 2- and 3-dimensional shapes, this one will be all about making those 2D designs a reality. The final goal, as mentioned in my documentation of week 2, is to produce a series of flat shapes which can be fitted together in order to form a 3D structure


###Time to find some inspiration
Anyone can waste a bunch of material to make a plain old cube, right? In an effort to make things a little more interesting, slightly less wasteful, and satisfy the secondary requirement of this assignment to include some form of engraving, I scoured the web for a good inspirational image of my favourite cube-shaped object of all time:



**The Aperture Science Weighted Companion Cube!**

<img src="/digital-fabrication-fundamentals/images/week03/CompanionCubeInspiration.png"  width="300" height="300">

Source: https://theportalwiki.com/wiki/File:Portal2_CompanionCube.png

Practically bursting with charm and charisma, this beloved icon of friendship and ray of hope in an otherwise intimidating world is the perfect inspiration for delving  deeper into the world of digital fabrication via laser cutting and engraving.

After a few shoddy attempts at creating my own approximation of the iconic cube's lovable faces, I resigned myself to using a pre-existing image courtesy of wikihow:

<img src="/digital-fabrication-fundamentals/images/week03/CompanionCubeSide.png"  width="300" height="300">

Source: https://www.wikihow.com/images/7/77/Companion-cube-pattern.jpg



###Let's get to the *_LASERS_* already!
The laser cutter has many similarities with modern printers (the 2D kind), employing a moving head to burn into or through that which is directly below it. Upon being provided with a specific type of image file, the machine directs its head to follow any lines within said image while outputting a steady amount of heat energy to a tiny point, thereby burning into or through and either engraving onto or cutting shapes out of materials like wood or plexiglas.

![GIF of laser cutter at work](../images/week03/LaserCutter1.gif)

Using a laser cutter requires some forethought, both while creating a design with joints for 3D shapes and instructing the machine how to handle the specific task you give it with regards to speed, power, and frequency of the photon beam.

![Second GIF of laser cutter at work](../images/week03/LaserCutter2.gif)

While designing with joints, you have to keep in mind how thick your material is and how much will be burned away during the cutting process; this amount is referred to as the **kerf**. Basically, you want your joints' extrusion length to be equal to material thickness + kerf, and also add the kerf measurement to the width of each protrusion to make sure it doesn't end up being too thin after the cut.  
When finally setting up your cut, the exact settings for the beam's strength depend on the material you want to use and how deeply you wish to burn into it. Also, the longer the beam remains in one spot, the deeper the "cut" will go; this means the depth of the cut is proportional to both **power** and **speed**. You will generally want to have lower power and higher speeds for engravings (which do net penetrate the material but merely burn away the surface) than for cuts (which need to burn all the way through the material).



###If at first you don't succeed...
<img src="/digital-fabrication-fundamentals/images/week03/FailedFaces1.jpg"  width="260" height="280">
<img src="/digital-fabrication-fundamentals/images/week03/CubeFacesV1.PNG"  width="500" height="250">

###...try and try again...

<img src="/digital-fabrication-fundamentals/images/week03/FailedFaces2.jpg"  width="260" height="300">
<img src="/digital-fabrication-fundamentals/images/week03/CubeFacesV2.PNG"  width="550" height="250">

###...and again and again again.

<img src="/digital-fabrication-fundamentals/images/week03/FailedFaces3.jpg"  width="260" height="300">
<img src="/digital-fabrication-fundamentals/images/week03/CubeFacesV3.PNG"  width="600" height="200">

###All's well that ends well!
After a **_long_** day of **_many_** failed attempts at making individual cube faces which actually fit together via joints, with several re-designs, a lot of research, and many additional hiccups along the way, I finally succeeded in cutting 6 sides of a cube which fit together *almost* perfectly. Some gentle cleaning with a wet towel helped to remove all of the brownish burned wood covering the cube, and I finally ended up with this beauty:

<img src="/digital-fabrication-fundamentals/images/week03/CubeSuccess.jpg"  width="300" height="270">
![GIF of successful companion cube fabrication](../images/week03/CubeSuccess.gif)


**_Files:_**


[Final cube face design](../files/week03/CompanionCubeFinal.dxf)