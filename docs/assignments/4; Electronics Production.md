#Electronics Production

###PCB Design
To begin fabricating a PCB, or Printed Circuit Board, one requires a functional schema. We'll get into that topic more in-depth in another assignment; in order to get going quickly, we were given a simple one for a so-called "HelloBoard", as shown below. 
<img src="/digital-fabrication-fundamentals/images/week04/HelloBoardDesign.jpg"  width="500" height="500">


###Board Milling
After transforming the design into image files and these image files into code which our mill can read, we were able to begin the actual fabrication process. The first step here was to set up the CNC mill correctly for our needs and have it cut our base board out of copper sheets.  
![](../images/week04/BoardMilling1.gif)  

As with laser cutting, it is generally best to cut the patterns into the surface of the board-to-be first, checking now and again for imperfections resulting from misalignments or such, then initiate the deeper outline cut.  
![](../images/week04/BoardMilling2.gif)  

The drill and settings required for these two different methods vary greatly and must be adjusted manually between steps. Also, it is advisable to (vacuum) clean the surface and check again whether the traces are wide enough before initiating the cutout process.  
![](../images/week04/BoardMilling3.gif)  

<img src="/digital-fabrication-fundamentals/images/week04/HelloBoardCutout.jpg"  width="150" height="150">

Before we could start affixing the many small parts to the board itself, we needed to make holes anywhere we wanted to add pins; this is best done with a smaller hand-drill rather than the CNC mill.  
####Before / After:  
<img src="/digital-fabrication-fundamentals/images/week04/HelloBoardNoHoles.jpg"  width="200" height="200">
<img src="/digital-fabrication-fundamentals/images/week04/HelloBoardWithHoles.jpg"  width="200" height="200">


###Soldering
Finally, this was where things got really interesting. Soldering, or sticking stuff to other stuff using super hot metal (mostly tin to be precise), allows us to combine our conductive copper board with many other tiny electronic parts to create our very own PCB with various functionality.
For the HelloBoard example, we will be focusing on a simple micro-controller, the ATTiny45SI, and a single-colored LED. These will be accompanied by a few capacitors, transistors, and pins.
<img src="/digital-fabrication-fundamentals/images/week04/SolderingMaterials.jpg"  width="300" height="300">

To achieve good connections and correct alignments for the tiny parts, it's imperative to hold them in place with a steady hand while feeding the line of soldering tin to the hot iron. Heating the copper board before-hand helps to ensure that the tin adheres to the board as well as whatever part is to be soldered to it. Also, checking the conductivity between points encapsulating the areas where you've added new parts will let you fix any broken connections before moving on to the next part and potentially boxing yourself in on the board, thus making it much harder to do later on.  
<img src="/digital-fabrication-fundamentals/images/week04/Soldering.jpg"  width="300" height="300">

###Board yet?
Here's the result of a (more or less) successful soldering session. With that taken care of, my very first self-made electronics board is complete and ready to go do... stuff.
<img src="/digital-fabrication-fundamentals/images/week04/HelloBoardComplete.jpg"  width="300" height="300">

With an Arduino board acting as a sort of interface between PCB and computer, it's possible to program the ATTiny Micro-chip on the board to control other active parts which it's connected to; in this case, an LED.
Time to make that baby blink!

##IT'S ALIVE!
![](../images/week04/BlinkingHelloBoard.gif)


**_Files:_**  
[Final cube face design](../files/week03/CompanionCubeFinal.dxf)