#Computer Assisted Design (CAD)

####CAD (not CAT!) refers to the use of software to create any kind of design.

The main software I used for my first steps in CAD is AutoDesk's "Fusion 360", which allows users to create both 2- and 3-dimensional designs. Since the long-term goal is to actually fabricate something physical (more on that later), it's important to ensure universally correct shapes and proportions rather than just drawing free-form; Fusion offers some excellent tools for doing so. The dimensions tool, for example, allows us to set exact lengths, distances, etc. of single or between multiple lines.  

##Designing in 2D

In the interest of being efficient and saving some time, my initial attempt at a 2D design is also what I will be my first attempt at working with the laser cutter. 

![Screenshot of a 2D design for sides of a 3D cube](../images/week02/cube_side_design.png)

The plan here is to create 6 instances of this design and connect them to make a  hollow 3D cube out of multiple 2D surfaces; again though, more on that in the laser cutting assignment doc.


##Adding a third dimension!

Once again striving to make efficient use of my time, I wanted my first 3D design to be something I could use for the additive fabrication assignment later on. Also, I figured I'd start off with a relatively simple shape and save myself a few bucks by self-producing the car-phone-holder for my that I'd been meaning to buy.  
Before I began with my design, I did some research on the exact measurements of my phone case (not just the phone itself) so I could use the holder without always having to remove the case first and putting it back on afterwards. According to the manufacturer of the case, the dimensions are 15.6 x 7.8 x 1 cm.


![Screenshot of a preliminary 3D design for a phone holder](../images/week02/phone_holder_base.png)
  
I then started by creating a 15.6 x 7.8 x 0.5 cm cuboid, which would serve as the base for my little contraption, choosing the 0.5 cm in hopes that this would be sufficiently thick to not break easily under pressure while also not being too bulky. Next, I added another couple of identical, parallel, thin cuboids onto the longer sides of the base; these were meant to become the actual holders. By merging the three cuboids and using Fusion 360's 'Draft' feature, I formed the protrusions in a way that would (hopefully) allow the plastic to give way slightly when pushing in the phone while still being strong enough to hold the phone once it was inside.

##Et voila!

To round out my design (pun absolutely intended), I put some curvature on all the otherwise sharp edges and corners in order to make the final item more user-friendly.

![Screenshot of a preliminary 3D design for a phone holder](../images/week02/phone_holder_design.png)

We'll end this chapter in our fabrication journey with a preliminary design for the primary structure of the phone holder. All that's missing now is some sort of support structure at the back of the base to actually fixate the entire thing to some part of the car, most likely the ventilation slats.

Will all this hard work pay off in the end? Is the design viable and ready for practical use as a 3D-printed prototype or will I end up destroying some super expensive 3D-printer?

Stay tuned to find out!


**_Files:_**  
[2D cube-face ](../files/week02/wooden_cube_side.f3d)  
[almost-certainly-perfect phone holder thing](../files/week02/phone_holder.f3z).