#First things first

####This section will cover how I set up the GitLab repository used for this project's documentation, which was actually our first assignment in the fundamentals course.

Having installed [GitForWindows](https://gitforwindows.org/), I launched the bash version of the software and navigated to my local Desktop directory via  `cd Desktop`. 

There I used the `git clone [link]` command in order to make a local copy of the previously initiated online GitLab repository named "digital fabrication fundamentals", which provided me with a local copy to work with and push to the remote repository using the `git commit` and `git push` functions respectively.

![Screenshot of the locally cloned directory in windows explorer and git bash](../images/week01/Repo_cloning.png)

After ensuring that everything was up to snuff using the `git status` command, I grabbed the master template graciously provided to us for creating our very own project documentation with the help of **mkdocs**, made several small changes and began banging out the very file that would become this page using [markdownpad](http://markdownpad.com/).

##Success!
After using `git add *` followed by `git commit` in **git bash** to index all the newly created, moved, deleted, or changed files I ran  `git push` in order to publish said changes to the remote **GitLab** repository, which automatically started the pipeline for generating a web page from the provided materials.

![First instance of the docs page](../images/week01/page_initialization.png)

After moving some things around to ensure this documentation wound up in its proper place in the  site's _Assignments_ tab and cleaning up a bit along the way, I was all set-up and ready to start my very own _**Digital Fabrication Journey!**_