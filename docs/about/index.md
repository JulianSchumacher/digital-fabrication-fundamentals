# About me

Hey there!

My name is **Julian Schumacher**, I am an _**E-Government**_ student taking part in the dual studies program at **Hochschule Rhein-Waal**. This means that while I'm studying a wide range of topics like _**Information Technologies**_, _**Software Development**_, _**Business Intelligence**_ and _**Project Management**_ in hopes of earning my Bachelor's degree, I'm also gathering practical experience working for a company; in my case, a large IT service provider for the federal government of North Rhine-Westphalia, **IT.NRW**!

My love of nerd culture goes way beyond programming though, and my biggest passion lies in video gaming!Thus, my real dream job is (unsurprisingly): 

_**Video Game Developer**_!

Hopefully I can redirect some of my creative urges to more physical media as well throughout the Digital Fabrication course. 

Wish me luck and **stay tuned**!